package com.capgemini.algorithmbattles.ballsapp.solution;

import com.capgemini.algorithmbattles.ballsapp.logic.model.Board;
import com.capgemini.algorithmbattles.ballsapp.logic.model.BoardCell;
import com.capgemini.algorithmbattles.ballsapp.logic.model.Player;

import java.util.*;
import java.util.stream.Collectors;

public class GamePlayer {

    private Player player;
    private Player opponent;
    private Board board = new Board();

    public GamePlayer(Player player) {
        this.player = player;
        this.opponent = (player == Player.PLAYER_1) ? Player.PLAYER_2 : Player.PLAYER_1;
    }

    /**
     * The application should calculate the next move after this method call.
     *
     * @return the next {@link BoardCell move} for current player.
     */
    public BoardCell nextMove() {
        System.out.println("Enter next move");
        //BoardCell cell = getCellForNextMove();
        Player[][] localBoard = board.getBoard();
        BoardCell cell = new BoardCell(-1, -1, player);
        if (Board.areMovesLeft(localBoard)) {
            cell = nextMoveCell(localBoard, player, opponent);
            cell.setPlayer(player);
            board.placeMove(cell);
        }
        System.out.println("Exit next move, cell:" + cell.getX() + " " + cell.getY());
        return cell;
    }

    private BoardCell getCellForNextMove() {
        // TODO: Please implement it.
        System.out.println("Enter get cell for next move");
        int bestVal = Integer.MIN_VALUE;
        BoardCell nextMove = new BoardCell(-1, -1, player);
        Player[][] localBoard = board.getBoard();

        for (int i = 0; i < Board.getSIZE(); i++) {
            for (int j = 0; j < Board.getSIZE(); j++) {
                if (localBoard[i][j] == null) {
                    localBoard[i][j] = player;
                    int moveVal = minimax(0, false, localBoard, player, opponent, -1000, 1000);
                    localBoard[i][j] = null;
                    if (bestVal < moveVal) {
                        nextMove.setX(i);
                        nextMove.setY(j);
                        bestVal = moveVal;
                    }
                }
            }
        }

        System.out.println("Exit get cell for next move, next move x:" + nextMove.getX() + ", y:" + nextMove.getY());
        return nextMove;
    }

    private static Map<BoardCell, Integer> collectFirstEmptySpace(Player[][] localBoard, Player player, Player opponent) {
        Map<BoardCell, Integer> possibleMoves = new HashMap<>();

        boolean found = false;
        for (int i = 0; i < localBoard.length && !found; i++) {
            for (int j = 0; j < localBoard.length && !found; j++) {
                if (localBoard[i][j] == null) {
                    possibleMoves.put(new BoardCell(i, j, player), 0);
                    found = true;
                }
            }
        }

        return possibleMoves;
    }

    public static Map<BoardCell, Integer> collectHorizontally(Player[][] localBoard, Player player, Player opponent) {
        Map<BoardCell, Integer> possibleMoves = new HashMap<>();

        int count = 0;
        Player currentPlayer = null;
        for (int i = 0; i < localBoard.length; i++) {
            for (int j = 0; j < localBoard.length; j++) {
                if (localBoard[i][j] == null) {
                    currentPlayer = null;
                } else {
                    if (localBoard[i][j].equals(currentPlayer)) {
                        System.out.println("Hey");
                        count++;
                        int weight;
                        if (count <= 3) {
                            weight = (currentPlayer == player) ? count*2 : count*2 + 1;
                        } else {
                            weight = (currentPlayer == player) ? count*2 + 1 : count*2;
                        }
                        if (0 <= j - count && localBoard[i][j - count] == null) {
                            possibleMoves.put(new BoardCell(i, j - count, player), weight);
                        }
                        if (j + 1 < localBoard.length && localBoard[i][j + 1] == null) {
                            possibleMoves.put(new BoardCell(i, j + 1, player), weight);
                        }
                    } else {
                        count = 1;
                        currentPlayer = localBoard[i][j];
                        if (j + 1 < 10 && localBoard[i][j+1] == null) {
                            possibleMoves.put(new BoardCell(i, j+1, player), 2);
                        }
                        if (0 < j - 1 && localBoard[i][j-1] == null) {
                            possibleMoves.put(new BoardCell(i, j-1, player), 2);
                        }
                    }
                }
            }
        }

        return possibleMoves;
    }

    public static Map<BoardCell, Integer> collectVertically(Player[][] localBoard, Player player, Player opponent) {
        Map<BoardCell, Integer> possibleMoves = new HashMap<>();

        Player currentPlayer = null;
        int count = 0;
        for (int i = 0; i < localBoard.length; i++) {
            for (int j = 0; j < localBoard.length; j++) {
                if (localBoard[j][i] == null) {
                    currentPlayer = null;
                } else {
                    if (localBoard[j][i].equals(currentPlayer)) {
                        count++;
                        int weight;
                        if (count <= 3) {
                            weight = (currentPlayer == player) ? count*2 : count*2 + 1;
                        } else {
                            weight = (currentPlayer == player) ? count*2 + 1 : count*2;
                        }
                        if (0 <= j - count && localBoard[j - count][i] == null) {
                            possibleMoves.put(new BoardCell(j - count, i, player), weight);
                        }
                        if (j + 1 < localBoard.length && localBoard[j + 1][i] == null) {
                            possibleMoves.put(new BoardCell(j + 1, i, player), weight);
                        }
                    } else {
                        count = 1;
                        currentPlayer = localBoard[j][i];
                        if (j+1 < 10 && localBoard[j+1][i] == null) {
                            possibleMoves.put(new BoardCell(j+1, i, player), 2);
                        }
                        if (0 < j-1 && localBoard[j-1][i] == null) {
                            possibleMoves.put(new BoardCell(j-1, i, player), 2);
                        }
                    }
                }
            }
        }

        return possibleMoves;
    }

    public static Map<BoardCell, Integer> collectDiagonally(Player[][] localBoard, Player player, Player opponent) {
        Map<BoardCell, Integer> possibleMoves = new HashMap<>();

        int min = (localBoard.length % 2 == 0) ? localBoard.length / 2 : localBoard.length;
        Player currentPlayer = null;
        int count = 0;
        for (int i = 0; i < localBoard.length; i++) {
            if (localBoard[i][i] == null) {
                currentPlayer = null;
            } else {
                if (localBoard[i][i].equals(currentPlayer)) {
                    count++;
                    int weight;
                    if (count <= 3) {
                        weight = (currentPlayer == player) ? count*2 : count*2 + 1;
                    } else {
                        weight = (currentPlayer == player) ? count*2 + 1 : count*2;
                    }
                    if (0 <= i - count && localBoard[i - count][i - count] == null) {
                        possibleMoves.put(new BoardCell(i - count, i - count, player), weight);
                    }
                    if (i + 1 < localBoard.length && localBoard[i + 1][i + 1] == null) {
                        possibleMoves.put(new BoardCell(i + 1, i + 1, player), weight);
                    }
                } else {
                    count = 1;
                    currentPlayer = localBoard[i][i];
                    if (0 < i - 1 && localBoard[i-1][i-1] == null) {
                        possibleMoves.put(new BoardCell(i-1, i-1, player), 2);
                    }
                    if (i+1 < 10 && localBoard[i+1][i+1] == null) {
                        possibleMoves.put(new BoardCell(i+1, i+1, player), 2);
                    }
                }
            }
        }
        currentPlayer = null;
        count = 0;
        for (int xOffset = 1; xOffset <= min; xOffset++) {
            for (int i = 0; i + xOffset < localBoard.length; i++) {
                if (localBoard[i][i + xOffset] == null) {
                    currentPlayer = null;
                } else {
                    if (localBoard[i][i + xOffset].equals(currentPlayer)) {
                        count++;
                        int weight;
                        if (count <= 3) {
                            weight = (currentPlayer == player) ? count*2 : count*2 + 1;
                        } else {
                            weight = (currentPlayer == player) ? count*2 + 1 : count*2;
                        }
                        if (0 <= i - count && localBoard[i - count][i + xOffset - count] == null) {
                            possibleMoves.put(new BoardCell(i - count, i + xOffset - count, player), weight);
                        }
                        if (i + xOffset + 1 < localBoard.length && localBoard[i + 1][i + xOffset + 1] == null) {
                            possibleMoves.put(new BoardCell(i + 1, i + xOffset + 1, player), weight);
                        }
                    } else {
                        count = 1;
                        currentPlayer = localBoard[i][i + xOffset];
                    }
                }
            }
        }
        currentPlayer = null;
        count = 0;
        for (int yOffset = 1; yOffset <= min; yOffset++) {
            for (int i = 0; i + yOffset < localBoard.length; i++) {
                if (localBoard[i + yOffset][i] == null) {
                    currentPlayer = null;
                } else {
                    if (localBoard[i + yOffset][i].equals(currentPlayer)) {
                        count++;
                        int weight;
                        if (count <= 3) {
                            weight = (currentPlayer == player) ? count*2 : count*2 + 1;
                        } else {
                            weight = (currentPlayer == player) ? count*2 + 1 : count*2;
                        }
                        if (0 <= i - count && localBoard[i + yOffset - count][i - count] == null) {
                            possibleMoves.put(new BoardCell(i + yOffset - count, i - count, player), weight);
                        }
                        if (i + yOffset + 1 < localBoard.length && localBoard[i + yOffset + 1][i + 1] == null) {
                            possibleMoves.put(new BoardCell(i + yOffset + 1, i + 1, player), weight);
                        }
                    } else {
                        count = 1;
                        currentPlayer = localBoard[i + yOffset][i];
                    }
                }
            }
        }

        return possibleMoves;
    }

    public static Map<BoardCell, Integer> collectAntiDiagonally(Player[][] localBoard, Player player, Player opponent) {
        Map<BoardCell, Integer> possibleMoves = new HashMap<>();

        int min = (localBoard.length % 2 == 0) ? localBoard.length / 2 : localBoard.length;
        Player currentPlayer = null;
        int count = 0;
        for (int i = localBoard.length - 1; 0 <= i; i--) {
            if (localBoard[localBoard.length - 1 - i][i] == null) {
                currentPlayer = null;
            } else {
                System.out.println("Here: " + (localBoard.length-1-i) + " " + i);
                if (localBoard[localBoard.length - 1 - i][i].equals(currentPlayer)) {
                    count++;
                    int weight;
                    if (count <= 3) {
                        weight = (currentPlayer == player) ? count*2 : count*2 + 1;
                    } else {
                        weight = (currentPlayer == player) ? count*2 + 1 : count*2;
                    }
                    if (0 <= localBoard.length-1-i-count && i+count < localBoard.length && localBoard[localBoard.length-1-i-count][i+count] == null) {
                        possibleMoves.put(new BoardCell(localBoard.length-1-i-count, i+count, player), weight);
                    }
                    if (0 < localBoard.length-i && 0 <= i - 1 && localBoard[localBoard.length-i][i-1] == null) {
                        possibleMoves.put(new BoardCell(localBoard.length-i, i-1, player), weight);
                    }
                } else {
                    count = 1;
                    currentPlayer = localBoard[localBoard.length - 1 - i][i];
                }
            }
        }
        currentPlayer = null;
        count = 0;
        for (int xOffset = 1; xOffset <= min; xOffset++) {
            for (int i = 0; i + xOffset < localBoard.length; i++) {
                if (localBoard[localBoard.length-1-i][i+xOffset] == null) {
                    currentPlayer = null;
                } else {
                    if (localBoard[localBoard.length-1-i][i+xOffset].equals(currentPlayer)) {
                        count++;
                        int weight;
                        if (count <= 3) {
                            weight = (currentPlayer == player) ? count*2 : count*2 + 1;
                        } else {
                            weight = (currentPlayer == player) ? count*2 + 1 : count*2;
                        }
                        if (0 <= i+xOffset-count && localBoard.length-1-i+count < localBoard.length && localBoard[localBoard.length-1-i+count][i+xOffset-count] == null) {
                            possibleMoves.put(new BoardCell(localBoard.length-1-i+count, i+xOffset-count, player), weight);
                        }
                        if (i+xOffset+1 < localBoard.length && localBoard[localBoard.length-2-i][i+xOffset+1] == null) {
                            possibleMoves.put(new BoardCell(localBoard.length-2-i, i+xOffset+1, player), weight);
                        }
                    } else {
                        count = 1;
                        currentPlayer = localBoard[localBoard.length-1-i][i+xOffset];
                    }
                }
            }
        }
        currentPlayer = null;
        count = 0;
        for (int yOffset = 1; yOffset <= min; yOffset++) {
            for (int i = 0; i + yOffset < localBoard.length; i++) {
                if (localBoard[localBoard.length-1-i-yOffset][i] == null) {
                    currentPlayer = null;
                } else {
                    System.out.println("i: " + (i) + " i+yOffset: " + (localBoard.length - 1 - i - yOffset));
                    if (localBoard[localBoard.length-1-i-yOffset][i].equals(currentPlayer)) {
                        count++;
                        int weight;
                        if (count <= 3) {
                            weight = (currentPlayer == player) ? count*2 : count*2 + 1;
                        } else {
                            weight = (currentPlayer == player) ? count*2 + 1 : count*2;
                        }
                        System.out.println("HEEEERE: " + i);
                        if (0 <= i-count && localBoard[localBoard.length-1-i-yOffset+count][i-count] == null) {
                            possibleMoves.put(new BoardCell(localBoard.length-1-i-yOffset+count, i-count, player), weight);
                            System.out.println("Adding: x: " + (i-count) + " y: " + (localBoard.length-1-i-yOffset+count) + " weight: " + weight);
                        }
                        if (0 <= localBoard.length-2-i-yOffset && localBoard[localBoard.length-2-i-yOffset][i+1] == null) {
                            possibleMoves.put(new BoardCell(localBoard.length-2-i-yOffset, i+1, player), weight);
                            System.out.println("Adding: x: " + (i+1) + " y: " + (localBoard.length-2-i-yOffset) + " weight: " + weight);
                        }
                    } else {
                        count = 1;
                        currentPlayer = localBoard[localBoard.length-1-i-yOffset][i];
                    }
                }
            }
        }

        return possibleMoves;
    }

    @SuppressWarnings("Duplicates")
    private static BoardCell nextMoveCell(Player[][] localBoard, Player player, Player opponent) {
        if (!Board.areMovesLeft(localBoard)) {
            return null;
        }

        Map<BoardCell, Integer> possibleMoves = new HashMap();
        List<Map<BoardCell, Integer>> allPossibleMoves = new LinkedList<>();

        //Find any empty place, if there's no more meaningful move
        allPossibleMoves.add(collectFirstEmptySpace(localBoard, player, opponent));

        //find any moves that could either block players or increase computers strike horizontally
        allPossibleMoves.add(collectHorizontally(localBoard, player, opponent));

        //find any moves that could either block players or increase computers strike vertically
        allPossibleMoves.add(collectVertically(localBoard, player, opponent));

        //find any moves that could either block players or increase computers strike diagonally
        allPossibleMoves.add(collectDiagonally(localBoard, player, opponent));

        //find any moves that could either block players or increase computers strike anti-diagonally
        allPossibleMoves.add(collectAntiDiagonally(localBoard, player, opponent));

        BoardCell bestMove = allPossibleMoves.stream().flatMap(map -> map.entrySet().stream())
                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue(), Integer::sum))
                .entrySet().stream()
                .max(Comparator.comparingInt(Map.Entry::getValue)).get().getKey();

        return bestMove;
    }

    public static int evaluation(Player[][] localBoard, Player player, Player opponent) {
        if (!Board.areMovesLeft(localBoard)) {
            return 0;
        }
        Player winner = Board.whoWon(localBoard);
        if (player.equals(winner)) {
            return 10;
        }
        if (opponent.equals(winner)) {
            return -10;
        }
        return 0;
    }

    public static int minimax(int depth, boolean isMax, Player[][] localBoard, Player player, Player opponent, int alpha, int beta) {

        //System.out.println(depth);

        /*if (depth == 3) {
            return 0;
        }*/

        int score = evaluation(localBoard, player, opponent);

        if (score == 10) {
            return 1000 - depth;
        }
        if (score == -10) {
            return -1000 - depth;
        }
        if (!Board.areMovesLeft(localBoard)) {
            return 0;
        }

        if (isMax) {
            int best = Integer.MIN_VALUE;
            for (int i = 0; i < localBoard.length; i++) {
                for (int j = 0; j < localBoard.length; j++) {
                    if (localBoard[i][j] == null) {
                        localBoard[i][j] = player;
                        best = Math.max(best, minimax(depth + 1, !isMax, localBoard, player, opponent, alpha, beta));
                        localBoard[i][j] = null;
                        alpha = Math.max(alpha, best);
                        if (beta <= alpha) {
                            break;
                        }
                    }
                }
            }
            return best;
        } else {
            int best = Integer.MAX_VALUE;
            for (int i = 0; i < localBoard.length; i++) {
                for (int j = 0; j < localBoard.length; j++) {
                    if (localBoard[i][j] == null) {
                        localBoard[i][j] = opponent;
                        best = Math.min(best, minimax(depth + 1, !isMax, localBoard, player, opponent, alpha, beta));
                        localBoard[i][j] = null;
                        alpha = Math.min(beta, best);
                        if (beta <= alpha) {
                            break;
                        }
                    }
                }
            }
            return best;
        }
    }

    /**
     * The opponent made the move passed in param.
     *
     * @param move the {@link BoardCell} made by the opponent.
     */
    public void moveMadeByOpponent(BoardCell move) {
        this.board.placeMove(move);
    }

    /**
     * @return true if the game is finished
     */
    public boolean isGameFinished() {
        return this.board.isGameFinished();
    }

    /**
     * Draw the board on the console.
     */
    public void printBoard() {
        this.board.printBoard();
    }
}
