package com.capgemini.algorithmbattles.ballsapp.logic;

import com.capgemini.algorithmbattles.ballsapp.logic.model.BoardCell;
import com.capgemini.algorithmbattles.ballsapp.logic.model.Player;
import com.capgemini.algorithmbattles.ballsapp.solution.GamePlayer;
import org.junit.Test;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * The tests for the game-application.
 */
public class GamePlayerTest {

    /**
     * This test checks if the application is able to have two instances and play against each other.
     */
    @Test
    public void shouldPlayGameWithItself() {
        GamePlayer gamePlayer1 = new GamePlayer(Player.PLAYER_1);
        GamePlayer gamePlayer2 = new GamePlayer(Player.PLAYER_2);

        while (!gamePlayer1.isGameFinished() && !gamePlayer2.isGameFinished()) {

            BoardCell move = gamePlayer1.nextMove();
            gamePlayer2.moveMadeByOpponent(move);
            gamePlayer1.printBoard();

            if (!gamePlayer2.isGameFinished()) {
                move = gamePlayer2.nextMove();
                gamePlayer1.moveMadeByOpponent(move);
                gamePlayer2.printBoard();
            }
        }
    }

    @Test
    public void testHorizontalScan() {
        Player[][] board = new Player[10][10];
        board[1][1] = Player.PLAYER_1;
        board[1][2] = Player.PLAYER_1;
        board[1][3] = Player.PLAYER_1;
        board[1][4] = Player.PLAYER_1;

        Map<BoardCell, Integer> possibleMoves = GamePlayer.collectHorizontally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[1][0] == 9 && [1][5] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }
    }

    @Test
    public void testVerticalScan() {
        Player[][] board = new Player[10][10];
        board[1][1] = Player.PLAYER_1;
        board[2][1] = Player.PLAYER_1;
        board[3][1] = Player.PLAYER_1;
        board[4][1] = Player.PLAYER_1;

        Map<BoardCell, Integer> possibleMoves = GamePlayer.collectVertically(board, Player.PLAYER_1, Player.PLAYER_2);

        //[0][1] == 9 && [5][1] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }
    }

    @Test
    public void testDiagonally() {
        Player[][] board = new Player[10][10];
        board[1][1] = Player.PLAYER_1;
        board[2][2] = Player.PLAYER_1;
        board[3][3] = Player.PLAYER_1;
        board[4][4] = Player.PLAYER_1;

        Map<BoardCell, Integer> possibleMoves = GamePlayer.collectDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[0][0] == 9 && [5][5] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("--------------");


        board = new Player[10][10];
        board[0][0] = Player.PLAYER_1;
        board[1][1] = Player.PLAYER_1;
        board[2][2] = Player.PLAYER_1;
        board[3][3] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[4][4] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("--------------");


        board = new Player[10][10];
        board[9][9] = Player.PLAYER_1;
        board[8][8] = Player.PLAYER_1;
        board[7][7] = Player.PLAYER_1;
        board[6][6] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[5][5] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("--------------");


        board = new Player[10][10];
        board[1][3] = Player.PLAYER_1;
        board[2][4] = Player.PLAYER_1;
        board[3][5] = Player.PLAYER_1;
        board[4][6] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[0][2] == 9 && [5][7] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("--------------");


        board = new Player[10][10];
        board[0][2] = Player.PLAYER_1;
        board[1][3] = Player.PLAYER_1;
        board[2][4] = Player.PLAYER_1;
        board[3][5] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[4][6] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("--------------");


        board = new Player[10][10];
        board[4][6] = Player.PLAYER_1;
        board[5][7] = Player.PLAYER_1;
        board[6][8] = Player.PLAYER_1;
        board[7][9] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[3][5] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("---------------");


        board = new Player[10][10];
        board[3][1] = Player.PLAYER_1;
        board[4][2] = Player.PLAYER_1;
        board[5][3] = Player.PLAYER_1;
        board[6][4] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[2][0] == 9 && [7][5] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("---------------");


        board = new Player[10][10];
        board[2][0] = Player.PLAYER_1;
        board[3][1] = Player.PLAYER_1;
        board[4][2] = Player.PLAYER_1;
        board[5][3] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[6][4] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("---------------");


        board = new Player[10][10];
        board[6][4] = Player.PLAYER_1;
        board[7][5] = Player.PLAYER_1;
        board[8][6] = Player.PLAYER_1;
        board[9][7] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[5][3] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }
    }

    @Test
    public void testAntiDiagonally() {
        Player[][] board = new Player[10][10];
        board[1][8] = Player.PLAYER_1;
        board[2][7] = Player.PLAYER_1;
        board[3][6] = Player.PLAYER_1;
        board[4][5] = Player.PLAYER_1;

        Map<BoardCell, Integer> possibleMoves = GamePlayer.collectAntiDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[0][9] == 9 && [5][4] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("-------------");


        board = new Player[10][10];
        board[0][9] = Player.PLAYER_1;
        board[1][8] = Player.PLAYER_1;
        board[2][7] = Player.PLAYER_1;
        board[3][6] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectAntiDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[4][5] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("-------------");


        board = new Player[10][10];
        board[6][3] = Player.PLAYER_1;
        board[7][2] = Player.PLAYER_1;
        board[8][1] = Player.PLAYER_1;
        board[9][0] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectAntiDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[5][4] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("-------------");


        board = new Player[10][10];
        board[1][7] = Player.PLAYER_1;
        board[2][6] = Player.PLAYER_1;
        board[3][5] = Player.PLAYER_1;
        board[4][4] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectAntiDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[0][8] == 9 && [5][3] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("-------------");


        board = new Player[10][10];
        board[0][8] = Player.PLAYER_1;
        board[1][7] = Player.PLAYER_1;
        board[2][6] = Player.PLAYER_1;
        board[3][5] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectAntiDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[4][4] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("-------------");


        board = new Player[10][10];
        board[5][3] = Player.PLAYER_1;
        board[6][2] = Player.PLAYER_1;
        board[7][1] = Player.PLAYER_1;
        board[8][0] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectAntiDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[4][4] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("-------------");


        board = new Player[10][10];
        board[5][5] = Player.PLAYER_1;
        board[6][4] = Player.PLAYER_1;
        board[7][3] = Player.PLAYER_1;
        board[8][2] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectAntiDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[9][1] == 9 && [4][6] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("-------------");


        board = new Player[10][10];
        board[6][4] = Player.PLAYER_1;
        board[7][3] = Player.PLAYER_1;
        board[8][2] = Player.PLAYER_1;
        board[9][1] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectAntiDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[5][5] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

        System.out.println("-------------");


        board = new Player[10][10];
        board[1][9] = Player.PLAYER_1;
        board[2][8] = Player.PLAYER_1;
        board[3][7] = Player.PLAYER_1;
        board[4][6] = Player.PLAYER_1;

        possibleMoves = GamePlayer.collectAntiDiagonally(board, Player.PLAYER_1, Player.PLAYER_2);

        //[5][5] == 9
        for (Map.Entry<BoardCell, Integer> entry : possibleMoves.entrySet()) {
            System.out.println("x: " + entry.getKey().getX() + " y: " + entry.getKey().getY() + " weight: " + entry.getValue());
        }

    }

}
