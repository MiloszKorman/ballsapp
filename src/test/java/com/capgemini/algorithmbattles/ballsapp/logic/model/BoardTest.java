package com.capgemini.algorithmbattles.ballsapp.logic.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class BoardTest {

    @Test
    public void whoWonTest() {
        //test only one player horizontally
        Player[][] board = new Player[10][10];
        board[0][0] = Player.PLAYER_1;
        board[0][1] = Player.PLAYER_1;
        board[0][2] = Player.PLAYER_1;
        board[0][3] = Player.PLAYER_1;
        board[0][4] = Player.PLAYER_1;

        assertEquals(Player.PLAYER_1, Board.whoWon(board));


        //test two players horizontally
        board = new Player[10][10];
        board[2][0] = Player.PLAYER_1;
        board[2][1] = Player.PLAYER_1;
        board[2][3] = Player.PLAYER_2;
        board[2][4] = Player.PLAYER_2;
        board[2][5] = Player.PLAYER_2;
        board[2][6] = Player.PLAYER_2;
        board[2][7] = Player.PLAYER_2;
        board[2][9] = Player.PLAYER_1;

        assertEquals(Player.PLAYER_2, Board.whoWon(board));


        //test one player vertically;
        board = new Player[10][10];
        board[0][0] = Player.PLAYER_1;
        board[1][0] = Player.PLAYER_1;
        board[2][0] = Player.PLAYER_1;
        board[3][0] = Player.PLAYER_1;
        board[4][0] = Player.PLAYER_1;

        assertEquals(Player.PLAYER_1, Board.whoWon(board));


        //test two players vertically
        board = new Player[10][10];
        board[0][3] = Player.PLAYER_1;
        board[1][3] = Player.PLAYER_1;
        board[3][3] = Player.PLAYER_2;
        board[4][3] = Player.PLAYER_2;
        board[5][3] = Player.PLAYER_2;
        board[6][3] = Player.PLAYER_2;
        board[7][3] = Player.PLAYER_2;
        board[9][3] = Player.PLAYER_1;

        assertEquals(Player.PLAYER_2, Board.whoWon(board));


        //test one player diagonally
        board = new Player[10][10];
        board[0][0] = Player.PLAYER_1;
        board[1][1] = Player.PLAYER_1;
        board[2][2] = Player.PLAYER_1;
        board[3][3] = Player.PLAYER_1;
        board[4][4] = Player.PLAYER_1;

        assertEquals(Player.PLAYER_1, Board.whoWon(board));


        //test two players diagonally
        board = new Player[10][10];
        board[1][0] = Player.PLAYER_1;
        board[2][1] = Player.PLAYER_1;
        board[3][2] = Player.PLAYER_2;
        board[4][3] = Player.PLAYER_2;
        board[5][4] = Player.PLAYER_2;
        board[6][5] = Player.PLAYER_2;
        board[7][6] = Player.PLAYER_2;
        board[9][8] = Player.PLAYER_2;

        assertEquals(Player.PLAYER_2, Board.whoWon(board));

        board = new Player[10][10];
        board[0][1] = Player.PLAYER_1;
        board[1][2] = Player.PLAYER_1;
        board[2][3] = Player.PLAYER_2;
        board[3][4] = Player.PLAYER_2;
        board[4][5] = Player.PLAYER_2;
        board[5][6] = Player.PLAYER_2;
        board[6][7] = Player.PLAYER_2;
        board[8][9] = Player.PLAYER_2;

        assertEquals(Player.PLAYER_2, Board.whoWon(board));


        //test one player anti-diagonally
        board = new Player[10][10];
        board[0][9] = Player.PLAYER_1;
        board[1][8] = Player.PLAYER_1;
        board[2][7] = Player.PLAYER_1;
        board[3][6] = Player.PLAYER_1;
        board[4][5] = Player.PLAYER_1;

        assertEquals(Player.PLAYER_1, Board.whoWon(board));


        //test two players anti-diagonally
        board = new Player[10][10];
        board[0][6] = Player.PLAYER_1;
        board[1][5] = Player.PLAYER_1;
        board[2][4] = Player.PLAYER_2;
        board[3][3] = Player.PLAYER_2;
        board[4][2] = Player.PLAYER_2;
        board[5][1] = Player.PLAYER_2;
        board[6][0] = Player.PLAYER_2;

        assertEquals(Player.PLAYER_2, Board.whoWon(board));

        board = new Player[10][10];
        board[3][9] = Player.PLAYER_1;
        board[4][8] = Player.PLAYER_1;
        board[5][7] = Player.PLAYER_2;
        board[6][6] = Player.PLAYER_2;
        board[7][5] = Player.PLAYER_2;
        board[8][4] = Player.PLAYER_2;
        board[9][3] = Player.PLAYER_2;

        assertEquals(Player.PLAYER_2, Board.whoWon(board));


        //test no-one winning
        board = new Player[10][10];
        board[0][0] = Player.PLAYER_1;
        board[0][1] = Player.PLAYER_1;
        board[0][2] = Player.PLAYER_1;
        board[0][3] = Player.PLAYER_1;
        board[3][4] = Player.PLAYER_2;
        board[4][5] = Player.PLAYER_2;
        board[6][6] = Player.PLAYER_1;
        board[7][7] = Player.PLAYER_2;

        assertNull(Board.whoWon(board));

    }

}