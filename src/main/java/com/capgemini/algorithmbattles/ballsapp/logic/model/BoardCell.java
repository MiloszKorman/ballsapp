package com.capgemini.algorithmbattles.ballsapp.logic.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class BoardCell {
    private int x;
    private int y;
    private Player player;

    @Override
    public boolean equals(Object obj) {
        if (player.equals(((BoardCell)obj).player)) {
            if (x == ((BoardCell) obj).x) {
                return (y == ((BoardCell) obj).y);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(x) + Integer.hashCode(y) + player.hashCode();
    }
}
