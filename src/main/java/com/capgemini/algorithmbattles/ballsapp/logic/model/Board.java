package com.capgemini.algorithmbattles.ballsapp.logic.model;

import com.capgemini.algorithmbattles.ballsapp.logic.BoardDrawer;

import java.util.Arrays;

public class Board {

    private static final int SIZE = 10;
    private Player[][] board = new Player[SIZE][SIZE];

    public void placeMove(BoardCell move) {
        board[move.getX()][move.getY()] = move.getPlayer();
    }

    public Player[][] getBoard() {
        return Arrays.copyOf(board, SIZE);
    }

    public static int getSIZE() {
        return SIZE;
    }

    public BoardCell getFirstEmptyCell() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (board[i][j] == null) {
                    return new BoardCell(i, j, null);
                }
            }
        }
        return null;
    }

    public boolean isGameFinished() {
        if (!areMovesLeft(board)) {
            return true;
        }
        Player winner = whoWon(board);
        if (winner == null) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isBoardEmpty(Player[][] localBoard) {
        for (Player[] row : localBoard) {
            for (Player colPlayer : row) {
                if (colPlayer != null) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean areMovesLeft(Player[][] localBoard) {
        for (Player[] row : localBoard) {
            for (Player colPlayer : row) {
                if (colPlayer == null) {
                    return true;
                }
            }
        }
        return false;
    }

    public static Player whoWon(Player[][] localBoard) {
        Player winner;
        winner = whoWonHorizontally(localBoard);
        if (winner != null) {
            return winner;
        }
        winner = whoWonVertically(localBoard);
        if (winner != null) {
            return winner;
        }
        winner = whoWonDiagonally(localBoard);
        if (winner != null) {
            return winner;
        }
        winner = whoWonAntiDiagonally(localBoard);
        if (winner != null) {
            return winner;
        }
        return null;
    }

    @SuppressWarnings("Duplicates")
    private static Player whoWonHorizontally(Player[][] localBoard) {
        int min = (localBoard.length%2 == 0) ? localBoard.length/2 : localBoard.length;
        Player currentPlayer = null;
        int playerInARow = 0;
        for (int x = 0; x < localBoard.length; x++) {
            for (int y = 0; y < localBoard.length; y++) {
                if (localBoard[x][y] == null) {
                    currentPlayer = null;
                } else {
                    if (localBoard[x][y].equals(currentPlayer)) {
                        playerInARow++;
                        if (min <= playerInARow) {
                            return currentPlayer;
                        }
                    } else {
                        playerInARow = 1;
                        currentPlayer = localBoard[x][y];
                    }
                }
            }
        }
        return null;
    }

    @SuppressWarnings("Duplicates")
    private static Player whoWonVertically(Player[][] localBoard) {
        int min = (localBoard.length%2 == 0) ? localBoard.length/2 : localBoard.length;
        Player currentPlayer = null;
        int playerInARow = 0;
        for (int y = 0; y < localBoard.length; y++) {
            for (int x = 0; x < localBoard.length; x++) {
                if (localBoard[x][y] == null) {
                    currentPlayer = null;
                } else {
                    if (localBoard[x][y].equals(currentPlayer)) {
                        playerInARow++;
                        if (min <= playerInARow) {
                            return currentPlayer;
                        }
                    } else {
                        playerInARow = 1;
                        currentPlayer = localBoard[x][y];
                    }
                }
            }
        }
        return null;
    }

    @SuppressWarnings("Duplicates")
    private static Player whoWonDiagonally(Player[][] localBoard) {
        int min = (localBoard.length%2 == 0) ? localBoard.length/2 : localBoard.length;
        Player currentPlayer = null;
        int playerInARow = 0;
        for (int i = 0; i < localBoard.length; i++) {
            if (localBoard[i][i] == null) {
                currentPlayer = null;
            } else {
                if (localBoard[i][i].equals(currentPlayer)) {
                    playerInARow++;
                    if (min <= playerInARow) {
                        return currentPlayer;
                    }
                } else {
                    playerInARow = 1;
                    currentPlayer = localBoard[i][i];
                }
            }
        }
        currentPlayer = null;
        playerInARow = 0;
        for (int xOffset = 1; xOffset <= min; xOffset++) {
            for (int i = 0; i + xOffset < localBoard.length; i++) {
                if (localBoard[i][i+xOffset] == null) {
                    currentPlayer = null;
                } else {
                    if (localBoard[i][i+xOffset].equals(currentPlayer)) {
                        playerInARow++;
                        if (min <= playerInARow) {
                            return currentPlayer;
                        }
                    } else {
                        playerInARow = 1;
                        currentPlayer = localBoard[i][i+xOffset];
                    }
                }
            }
        }
        currentPlayer = null;
        playerInARow = 0;
        for (int yOffset = 1; yOffset <= min; yOffset++) {
            for (int i = 0; i + yOffset < localBoard.length; i++) {
                if (localBoard[i+yOffset][i] == null) {
                    currentPlayer = null;
                } else {
                    if (localBoard[i+yOffset][i].equals(currentPlayer)) {
                        playerInARow++;
                        if (min <= playerInARow) {
                            return currentPlayer;
                        }
                    } else {
                        playerInARow = 1;
                        currentPlayer = localBoard[i+yOffset][i];
                    }
                }
            }
        }
        return null;
    }

    @SuppressWarnings("Duplicates")
    private static Player whoWonAntiDiagonally(Player[][] localBoard) {
        //todo implement
        int min = (localBoard.length%2 == 0) ? localBoard.length/2 : localBoard.length;
        Player currentPlayer = null;
        int playerInARow = 0;
        for (int i = localBoard.length-1; 0 <= i; i--) {
            if (localBoard[localBoard.length-1-i][i] == null) {
                currentPlayer = null;
            } else {
                if (localBoard[localBoard.length-1-i][i].equals(currentPlayer)) {
                    playerInARow++;
                    if (min <= playerInARow) {
                        return currentPlayer;
                    }
                } else {
                    playerInARow = 1;
                    currentPlayer = localBoard[localBoard.length-1-i][i];
                }
            }
        }
        currentPlayer = null;
        playerInARow = 0;
        for (int xOffset = 1; xOffset <= min; xOffset++) {
            for (int i = 0; i + xOffset < localBoard.length; i++) {
                if (localBoard[localBoard.length-1-i][i+xOffset] == null) {
                    currentPlayer = null;
                } else {
                    if (localBoard[localBoard.length-1-i][i+xOffset].equals(currentPlayer)) {
                        playerInARow++;
                        if (min <= playerInARow) {
                            return currentPlayer;
                        }
                    } else {
                        playerInARow = 1;
                        currentPlayer = localBoard[localBoard.length-1-i][i+xOffset];
                    }
                }
            }
        }
        currentPlayer = null;
        playerInARow = 0;
        for (int yOffset = 1; yOffset <= min; yOffset++) {
            for (int i = 0; i + yOffset < localBoard.length; i++) {
                if (localBoard[localBoard.length-1-i-yOffset][i] == null) {
                    currentPlayer = null;
                } else {
                    if (localBoard[localBoard.length-1-i-yOffset][i].equals(currentPlayer)) {
                        playerInARow++;
                        if (min <= playerInARow) {
                            return currentPlayer;
                        }
                    } else {
                        playerInARow = 1;
                        currentPlayer = localBoard[localBoard.length-1-i-yOffset][i];
                    }
                }
            }
        }
        return null;
    }

    public void printBoard() {
        BoardDrawer.drawBoard(this.board);
    }

}
